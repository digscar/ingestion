# Flexible Message Ingestion
Smart contract for ingesting messages in a flexible manner.

## Quick Start
``` bash
export PRIVATE_KEY=5JDFdVGeQdmpz4tQw3sRpvqYHgpMd3Rp6FgJfZ27RJw4qRUzWww  (double-check your key)
cd scripts && npm install && node ingestion.js -f messages/eligible_msg.json -c ingestion --ingest
```
And then check the messages:
``` bash
cleos $HOST get table -r ingestion ingestion messages
```

## Building Contract (Ubuntu)
Ensure that eosio.cdt is installed 
>NOTE: 1.7.0 is latest as of 12 Mar 2020; check if much later: https://github.com/EOSIO/eosio.cdt/releases
```
wget https://github.com/eosio/eosio.cdt/releases/download/v1.7.0/eosio.cdt-1.7.0-1.el7.x86_64.rpm
sudo apt install ./eosio.cdt_1.7.0-1-ubuntu-18.04_amd64.deb
cmake . && make
```

## Data Structure
There are two examples in the smart contract, general and specific.

The general purpose ingestion method is an action with the following message signature:

``` c++
ACTION ingestgen (  const map<string, name> 		    names,
                    const map<string, string>       strings,
                    const map<string, asset>        assets,
                    const map<string, time_point>   time_points,
                    const map<string, uint64_t>     ints,
                    const map<string, float>        floats);
```

It accepts name/value pairs for all of the core eosio data types.  

As an example, it could accept and store the following JSON object.

``` json
{
  "data": {
    "names": [
      {
        "key": "game_type",
        "value": "xxxxxxxx"
      },
      {
        "key": "msg_type",
        "value": "create_entry"
      }
    ],
    "strings": [
      {
        "key": "location",
        "value": "XYZ Main St, Anywhere, ETC 78456"
      },
      {
        "key": "description",
        "value": "This could be a really long string description."
      }
    ],
    "assets": [
      {
        "key": "entry_cost",
        "value": "1.00 AUD"
      },
      {
        "key": "lott_used",
        "value": "0.50 LOTT"
      },
      {
        "key": "lott_earned",
        "value": "0.01 LOTT"
      }
    ],
    "time_points": [
      {
        "key": "purchase_date",
        "value": "2020-01-28T14:30:58.000"
      }
    ],
    "ints": [
      {
        "key": "entry_number1",
        "value": "23"
      },
      {
        "key": "entry_number2",
        "value": "2"
      },
      {
        "key": "entry_number3",
        "value": "17"
      },
      {
        "key": "entry_number4",
        "value": "29"
      },
      {
        "key": "entry_number5",
        "value": "6"
      },
      {
        "key": "entry_number6",
        "value": "11"
      }
    ],
    "floats": []
  }
}
```

## Web Service Endpoint
There is also a web service endpoint to ingest the messages. You can invoke it with the following curl command.

``` bash
curl --location --request POST 'https://jedbns8pqb.execute-api.us-east-1.amazonaws.com/dev/v1/messages' \
--header 'Content-Type: application/json' \
--data-raw '{
	"host": "https://test.telos.kitchen",
    "contract": "ingestionaaa",
    "data": {
    	"names": [
            {
                "key": "game_type",
                "value": "xxxxxxxxx"
            },
            {
                "key": "msg_type",
                "value": "create_entry"
            }
        ],
        "strings": [
            {
                "key": "location",
                "value": "XYZ Main St, Anywhere, ETC 78456"
            },
            {
                "key": "description",
                "value": "This could be a really long string description."
            }
        ],
        "assets": [
            {
                "key": "entry_cost",
                "value": "1.00 AUD"
            },
            {
                "key": "lott_used",
                "value": "0.50 LOTT"
            },
            {
                "key": "lott_earned",
                "value": "0.01 LOTT"
            }
        ],
        "time_points": [
            {
                "key": "purchase_date",
                "value": "2020-01-28T14:30:58.000"
            }
        ],
        "ints": [  
            {
                "key": "entry_number1",
                "value": "23"
            },
            {
                "key": "entry_number2",
                "value": "2"
            },
            {
                "key": "entry_number3",
                "value": "17"
            },
            {
                "key": "entry_number4",
                "value": "29"
            },
            {
                "key": "entry_number5",
                "value": "6"
            },
            {
                "key": "entry_number6",
                "value": "11"
            }
        ],
        "floats": []
    }
}'
```

## Eligibility Filtering
The contract includes an action to illustrate a simple method for filtering records to include only eligible records. The specific business rules are not implemented, but the example illustrates how to test against each data type. 

If the record is eligible, it is **moved** to the ```eligible``` scope of the ```messages``` table.  

If the record is NOT eligible, it is erased.

Since many messages cannot be processed within a single transaction, the ```process``` action uses deferred transactions to process records. It divides the work into batches of 10 records (although this should be evaluated to optimize for performance - it can be higher).  When it reaches the batch size, it initiates another deferred transaction before existing. It chains together as many transactions as it takes until all of the records are processed. 

NOTE: B1 has indicated that deferred transaction may be deprecated in the future. If that happens, we can just move this part of the service off chain.
