const { Api, JsonRpc, RpcError } = require("eosjs");
const { JsSignatureProvider } = require("eosjs/dist/eosjs-jssig"); 
const { TextEncoder, TextDecoder } = require("util");
const fetch = require("node-fetch"); // node only; not needed in browsers
const defaultPrivateKey = process.env.privateKey;
const signatureProvider = new JsSignatureProvider([defaultPrivateKey]);

function respond(httpCode, body) {
  return buildResponse(httpCode, body);
}

function buildResponse(statusCode, body) {
  return {
    statusCode: statusCode,
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Credentials": true
    },
    body: JSON.stringify(body)
  };
}

async function sendtrx(host, contract, action, authorizer, data) {
  // console.log ("fetch     : ", fetch);
  // console.log ("typeof fetch  : ", typeof fetch);
  const rpc = new JsonRpc(host, { fetch });
  const api = new Api({ rpc, signatureProvider, textDecoder: new TextDecoder(), textEncoder: new TextEncoder() });
  const actions = [{ account: contract, name: action, authorization: [{ actor: authorizer, permission: "active" }], data: data }];
  const result = await api.transact({ actions: actions }, { blocksBehind: 3, expireSeconds: 30 });
  console.log("Transaction Successfull : ", result.transaction_id);
  return result.transaction_id;
}

export async function main(event, context) {
  const data = JSON.parse(event.body);
  console.log ("Host      : ", data.host);
  console.log ("Contract  : ", data.contract);
  console.log ("Data      : ", JSON.stringify(data.data, null, 2));
  console.log ("PK        : ", defaultPrivateKey);
  try {
    const transaction_id = await sendtrx(data.host, data.contract, "ingestgen", data.contract, data.data);
    return respond(200, { message: `Ingestion successful. Transaction ID: ${transaction_id}` });
  } catch (e) {
    console.log (e);
    return respond(500, { message: e.message });
  }
}
