// require('dotenv').config()

const express = require('express')
var bodyParser = require('body-parser')
var app = express()
// parse application/json
app.use(bodyParser.json())

const PRIVATE_KEY = process.env.PRIVATE_KEY;
const PORT = process.env.PORT;

async function sendtrx(host, contract, action, authorizer, data) {

  const { Api, JsonRpc } = require("eosjs");
  const { JsSignatureProvider } = require("eosjs/dist/eosjs-jssig"); 
  const { TextEncoder, TextDecoder } = require("util"); 
  const fetch = require("node-fetch"); // node only; not needed in browsers
  const signatureProvider = new JsSignatureProvider([PRIVATE_KEY]);

  const rpc = new JsonRpc(host, { fetch });
  const api = new Api({ rpc, signatureProvider, textDecoder: new TextDecoder(), textEncoder: new TextEncoder() });
  const actions = [{ account: contract, name: action, authorization: [{ actor: authorizer, permission: "active" }], data: data }];
  const result = await api.transact({ actions: actions }, { blocksBehind: 3, expireSeconds: 30 });
  return result.transaction_id;
}

async function getLastMessage (host, contract) {
  const { JsonRpc } = require("eosjs");
  const fetch = require("node-fetch"); // node only; not needed in browsers
  const rpc = new JsonRpc(host, { fetch });

  let options = {};
  options.code = contract;
  options.scope = contract; 

  options.json = true;
  options.scope = contract;
  options.table = "messages";
  options.reverse = true;
  options.limit = 1;
  
  const result = await rpc.get_table_rows(options);
  if (result.rows.length > 0) {
    return result.rows[0];
  } 
  
  throw new Error (`No messages exist at ${host} in contract ${contract}`);
}

app.post('/v1/messages', function (req, res) {
  sendtrx(req.body.host, req.body.contract, "ingestgen", req.body.contract, req.body.data).then ( (transaction_id) => {
    console.log (`Ingestion successful. Transaction ID: ${transaction_id}`);
    res.status(200).send({ message: `Ingestion successful. Transaction ID: ${transaction_id}` });
  }).catch ((e) => {
    console.log (e);
    res.status(500).send({ message: e.message });
  })
})

app.get('/v1/messages', function (req, res) {
  getLastMessage (req.body.host, req.body.contract).then ( (message) => {
    console.log ('Fetched last message, returning with response.');
    res.status(200).send({ message });
  }).catch ((e) => {
    console.log (e);
    res.status(500).send({ message: e.message });
  })
})

app.listen(PORT, () => console.log(`eosio Ingestion Engine listening on port: ${PORT}`))