# To run container

## Step 1: Create environment vars (or skip this if someone provided them):
``` bash
cat > .env <<EOF
PRIVATE_KEY=<update to your private key>
PORT=3000
EOF
```

## Step 2: Run container
``` bash
docker run --env-file=.env -p 3000:3000 dsmaxds/ingestion
```

## Step 3: Test Service
``` bash
curl --location --request POST 'localhost:3000/v1/messages' \
--header 'Content-Type: application/json' \
--data-raw '{
	"host": "https://test.telos.kitchen",
    "contract": "ingestionaaa",
    "data": {
    	"names": [
            {
                "key": "sample_attribute",
                "value": "sampvalue"
            },
            {
                "key": "sample_attribute_number_2",
                "value": "createentry"
            }
        ],
        "strings": [
            {
                "key": "location",
                "value": "XYZ Main St, Anywhere, ETC 78456"
            },
            {
                "key": "description",
                "value": "This could be a really long string description."
            }
        ],
        "assets": [
            {
                "key": "price",
                "value": "12.00 AUD"
            }
        ],
        "time_points": [
            {
                "key": "purchase_date",
                "value": "2020-01-28T14:30:58.000"
            }
        ],
        "ints": [],
        "floats": []
    }
}'
```

# To build container
``` bash
export IMAGE_NAME=myregistry/myimage
git clone https://gitlab.com/digscar/ingestion
cd ingestion/container
docker build -t $IMAGE_NAME .
```

# To push container
``` bash
docker push IMAGE_NAME:latest
```