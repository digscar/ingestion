const commandLineArgs = require("command-line-args");
const fs = require('fs');

const { Api, JsonRpc } = require("eosjs");
const { JsSignatureProvider } = require("eosjs/dist/eosjs-jssig");
const fetch = require("node-fetch");
const { TextEncoder, TextDecoder } = require("util");

const defaultPrivateKey = process.env.PRIVATE_KEY;
const signatureProvider = new JsSignatureProvider([defaultPrivateKey]);

async function sendtrx(host, contract, action, authorizer, data) {
  console.log ("Submitting with NONCE: ", data.nonce)
  const rpc = new JsonRpc(host, { "fetch": fetch });
  const api = new Api({ rpc, signatureProvider, textDecoder: new TextDecoder(), textEncoder: new TextEncoder() });
  const actions = [{ account: contract, name: action, authorization: [{ actor: authorizer, permission: "active" }], data: data }];
  const result = await api.transact({ actions: actions }, { blocksBehind: 3, expireSeconds: 30 });
  console.log("Transaction Successful : ", result.transaction_id);
  return result.transaction_id;
}

class FileDetails {
  constructor (filename) {
    this.filename = filename
    this.exists = fs.existsSync(filename)
  }
}

async function loadOptions() {
  const optionDefinitions = [
    { name: "file", alias: "f", type: filename => new FileDetails(filename) },
    { name: "host", alias: "h", type: String, defaultValue: "http://T17DC1LEAP02.tattsnonprod.com:8888" },
    { name: "ingest", alias: "i", type: Boolean, defaultValue: true },
    { name: "contract", alias: "c", type: String, defaultValue: "ingestion"},
    { name: "config", type: Boolean, defaultValue: false },
    { name: "loop", alias: "l", type: Number, defaultValue: 0},
    { name: "process", alias: "p", type: Boolean, defaultValue: false }
	  // see here to add new options:
    //   - https://github.com/75lb/command-line-args/blob/master/doc/option-definition.md
  ];
  return commandLineArgs(optionDefinitions);
}

const main = async () => {
  const opts = await loadOptions();

  if (opts.config) {
    console.log ("\nConfiguration actions have not yet been implemented. Ignoring --config.");
  }

  // setting configuration
  if (opts.file && opts.ingest) {
    const msg = JSON.parse(fs.readFileSync(opts.file.filename, 'utf8'));
    console.log ("\nParsing the message file & submitting to chain. File : ", opts.file.filename);
    nonce = Math.floor(Math.random() * Math.floor(10000000));
    for (var i=0; i <= opts.loop; i++) {
      nonce += i
      msg.data.nonce = nonce
      trxId = await sendtrx(opts.host, opts.contract, "ingestgen", opts.contract, msg.data);
    }
  } else {
    console.log ("Unsupported invocation.");
  }  
}

main()
