## Ingestion Nodejs Script

The ```ingestion.js``` script supports different mechanisms to interact with the ingestion contract. 

## Usage

Ingest a message:
```
node scripts/ingestion.js --file scripts/messages/eligible_msg.json --ingest
```

Use a node other than T17:
```
node scripts/ingestion.js --file scripts/messages/eligible_msg.json --ingest --host http://<node>.tattsgroup.com:8888
```

Use a contract other than ingestion:
```
node scripts/ingestion.js --file scripts/messages/eligible_msg.json --ingest --contract mycontract
```

Submit a message 100 times:
```
node scripts/ingestion.js --file scripts/messages/eligible_msg.json --ingest --loop 100
```