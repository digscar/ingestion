#include <eosio/eosio.hpp>
#include <eosio/multi_index.hpp>

using namespace eosio;
using std::string;

class Entrants
{
public:                                                                          
        struct [[eosio::table, eosio::contract("ingestion") ]] Entrant
    {
        uint64_t entrant_id;
        string entrant_name;

        uint64_t draw_id;
        time_point created_date;
        // .......
        //
        uint64_t primary_key() const { return entrant_id; }
        uint64_t by_draw()      const { return draw_id; }
    };

    typedef multi_index<"entrants"_n, Entrant> entrant_table;

    name contract;
    entrant_table entrant_t;

    Entrants(const name &contract) : contract(contract),
                                 entrant_t(contract, contract.value) {}

    uint64_t create_entrant(const uint64_t& draw_id,  
                            const string& entrant_name)
    {
        uint64_t entrant_id = entrant_t.available_primary_key();

        entrant_t.emplace(contract, [&](auto &e) {
            e.entrant_id = entrant_id;
            e.entrant_name = entrant_name;
            e.draw_id  = draw_id;
            e.created_date = current_time_point();
        });
        return entrant_id;
    }
};