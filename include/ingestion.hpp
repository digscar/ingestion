#include <eosio/eosio.hpp>
#include <eosio/asset.hpp>
#include <eosio/transaction.hpp>
#include <eosio/singleton.hpp>

#include "draw.hpp"
#include "entrant.hpp"

using namespace eosio;
using std::map;
using std::string;
using std::set;

CONTRACT ingestion : public contract {
   public:
      using contract::contract;

      struct [[eosio::table, eosio::contract("ingestion") ]] Config 
      {
         map<string, name>          names             ;
         map<string, string>        strings           ;
         map<string, asset>         assets            ;
         map<string, time_point>    time_points       ;
         map<string, uint64_t>      ints              ;
         map<string, transaction>   trxs              ;
         map<string, float>         floats            ;
      };

      typedef singleton<"config"_n, Config> config_table;
      typedef multi_index<"config"_n, Config> config_table_placeholder;


      // *****  General Purpose Ingestion ****
      // generic data structure to enable storage of all 
      // eosio core data types with strings as keys for each data element
      struct [[eosio::table, eosio::contract("ingestion") ]] Message
      {
         uint64_t                   id                ;
         
         // core data types
         map<string, name>          names             ;  // [a-z][1-5] - 12 or fewer characters
         map<string, string>        strings           ;
         map<string, asset>         assets            ;
         map<string, time_point>    time_points       ;
         map<string, uint64_t>      ints              ;
         map<string, float>         floats            ;
         uint64_t                   primary_key()     const { return id; }

         time_point                 created_date    = current_time_point();
         uint64_t    by_created () const { return created_date.sec_since_epoch(); }
         uint64_t    by_type () const { return names.at("type").value; }
      };

      typedef multi_index<"messages"_n, Message,
         indexed_by<"bycreated"_n, const_mem_fun<Message, uint64_t, &Message::by_created>>,
         indexed_by<"bytype"_n, const_mem_fun<Message, uint64_t, &Message::by_type>>
      > message_table;

      // This is a very generic ingestion action that supports 
      // nearly all eosio data types
      ACTION ingestgen (const uint64_t                  nonce,
                        const map<string, name> 		  names,
                        const map<string, string>       strings,
                        const map<string, asset>        assets,
                        const map<string, time_point>   time_points,
                        const map<string, uint64_t>     ints,
                        const map<string, float>        floats);
      // *****  END: General Purpose Ingestion ****

      static const uint64_t       MICROSECONDS_PER_HOUR   = (uint64_t)60 * (uint64_t)60 * (uint64_t)1000000;
      static const uint64_t       MICROSECONDS_PER_YEAR   = MICROSECONDS_PER_HOUR * (uint64_t)24 * (uint64_t)365;
      ACTION process ();
      ACTION process (const uint64_t start_id, const uint64_t batch_size);
      ACTION remmsg (const name& scope);

   private:
      Draws draws = Draws (get_self());
      Entrants entrants = Entrants(get_self());

      uint64_t get_next_sender_id ();
      void add_to_eligible(const name &current_scope, const uint64_t &id, const name &new_scope, const bool &remove_old);
};
