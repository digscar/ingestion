#include <eosio/eosio.hpp>
#include <eosio/multi_index.hpp>

using namespace eosio;
using std::string;

class Draws
{
public:
        struct [[eosio::table, eosio::contract("ingestion") ]] Draw
        {
            uint64_t draw_id;
            string draw_name;
            time_point created_date;
            // .......
            //
            uint64_t primary_key() const { return draw_id; }
        };

        typedef multi_index<"draws"_n, Draw> draw_table;

        name contract;
        draw_table draw_t;

        Draws(const name &contract) : contract(contract),
                                     draw_t(contract, contract.value) {}

        uint64_t create_draw(const string &draw_name)
        {
            uint64_t did = draw_t.available_primary_key();

            draw_t.emplace(contract, [&](auto &d) {
                d.draw_id = did;
                d.draw_name = draw_name;
                d.created_date = current_time_point();
            });
            return did;
        }
};