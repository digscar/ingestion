#include <ingestion.hpp>

void ingestion::ingestgen(const uint64_t nonce,
                          const map<string, name> names,
                          const map<string, string> strings,
                          const map<string, asset> assets,
                          const map<string, time_point> time_points,
                          const map<string, uint64_t> ints,
                          const map<string, float> floats)
{

   message_table m_t(get_self(), get_self().value);
   m_t.emplace(get_self(), [&](auto &m) {
      m.id = m_t.available_primary_key();
      m.names = names;
      m.strings = strings;
      m.assets = assets;
      m.time_points = time_points;
      m.ints = ints;
      m.floats = floats;
   });
}

void ingestion::remmsg (const name& scope) {
   message_table m_t (get_self(), scope.value);
   auto m_itr = m_t.begin();
   while (m_itr != m_t.end()) {
      m_itr = m_t.erase (m_itr);
   }
}

// Check eligibility and move records to the eligible scope or erase
void ingestion::process() {
   message_table m_t(get_self(), get_self().value);
   auto m_itr = m_t.begin();
   // batch size of 10 can be increased until transactions start to fail
   process (m_itr->id, 10);
}

void ingestion::process(const uint64_t start_id, const uint64_t batch_size)
{
   // Pretend Eligibility Criteria:
   // string of "eligible" set to "I am eligible"
   // asset of "amount" >= 1.0000 ELIG
   // time_points "dob" < 18 years ago
   // int named "myint" == 42
   // float named "myfloat" > 3.14

   message_table m_t(get_self(), get_self().value);
   auto m_itr = m_t.find(start_id);
   
   while (m_itr != m_t.end() && m_itr->id <= start_id + batch_size)
   {
      if (
          m_itr->strings.find("eligible") != m_itr->strings.end() &&          // "eligible" exists as string
          m_itr->strings.at("eligible") == string("I am eligible") &&         // equal to "I am eligible"

          m_itr->assets.find("amount") != m_itr->assets.end() &&              // "amount" exists as asset
          m_itr->assets.at("amount").symbol == symbol("ELIG", 4) &&           // has symbol of ELIG, 4
          m_itr->assets.at("amount").amount >= 10000 &&                       // >= 1.0000 ELIG

          m_itr->time_points.find("dob") != m_itr->time_points.end() &&       // "dob" exists as time_point
          m_itr->time_points.at("dob").time_since_epoch() <=                  // over 18 years ago
              (current_block_time().to_time_point().time_since_epoch() - 
                microseconds(18 * MICROSECONDS_PER_YEAR)) &&

          m_itr->ints.find("myint") != m_itr->ints.end() &&                   // "myint" exists as int
          m_itr->ints.at("myint") == 42 &&                                    // equal to 42

          m_itr->floats.find("myfloat") != m_itr->floats.end() &&             // "myfloat" exists as float
          m_itr->floats.at("myfloat") > 3.14)                                 // greater than 3.14
      {
         // record is eligible
         add_to_eligible(get_self(), m_itr->id, "eligible"_n, false);

         // create draw and entrant
         entrants.create_entrant(draws.create_draw(m_itr->strings.at("draw_name")), m_itr->strings.at("entrant_name"));
      }
      m_itr = m_t.erase(m_itr);
   }

   // if we aren't at the end of the table, kick off another transaction to process 
   // the next batch
   if (m_itr != m_t.end())
   {
      eosio::transaction out{};
      out.actions.emplace_back(permission_level{get_self(), "active"_n},
                               get_self(), "process"_n,
                               std::make_tuple(m_itr->id, batch_size));
      // out.delay_sec = 1;  - it is possible to add a delay but we're not

      // we need to increment the sender_id, which just acts as a nonce
      out.send(get_next_sender_id(), get_self());
   }
}

uint64_t ingestion::get_next_sender_id()
{
   config_table config_s(get_self(), get_self().value);
   Config c = config_s.get_or_create(get_self(), Config());
   uint64_t return_senderid = c.ints.at("last_sender_id");
   return_senderid++;
   c.ints["last_sender_id"] = return_senderid;
   config_s.set(c, get_self());
   return return_senderid;
}

void ingestion::add_to_eligible(const name &current_scope, const uint64_t &id, const name &new_scope, const bool &remove_old)
{

   message_table m_t_current(get_self(), current_scope.value);
   auto m_itr_current = m_t_current.find(id);
   check(m_itr_current != m_t_current.end(), "Scope: " + current_scope.to_string() + "; Message ID: " + std::to_string(id) + " does not exist.");

   message_table m_t_new(get_self(), new_scope.value);
   m_t_new.emplace(get_self(), [&](auto &m) {
      m.id = m_t_new.available_primary_key();
      m.names = m_itr_current->names;
      m.names["prior_scope"] = current_scope;
      m.assets = m_itr_current->assets;
      m.strings = m_itr_current->strings;
      m.floats = m_itr_current->floats;
      m.time_points = m_itr_current->time_points;
      m.ints = m_itr_current->ints;
      m.ints["prior_id"] = m_itr_current->id;
   });

   if (remove_old)
   {
      m_t_current.erase(m_itr_current);
   }
}

