cmake_minimum_required(VERSION 3.7)

project(ingestion)

set(EOSIO_WASM_OLD_BEHAVIOR "Off")
find_package(eosio.cdt)

add_contract( ingestion ingestion ingestion.cpp )
target_include_directories( ingestion PUBLIC ${CMAKE_SOURCE_DIR}/../include )
